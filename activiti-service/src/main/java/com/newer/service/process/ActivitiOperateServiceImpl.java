/**
 * 
 */
package com.newer.service.process;

import java.io.InputStream;
import java.util.zip.ZipInputStream;

import org.activiti.engine.repository.Deployment;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.newer.BaseBizFactory;
import com.newer.integration.process.ActivitiOperateService;
import com.newer.result.BaseResult;

/**
 * 流程操作相关服务实现
 * 
 * @author Administrator
 */
public class ActivitiOperateServiceImpl implements ActivitiOperateService {

	// 日志信息打印
	private static final Logger logger = LoggerFactory.getLogger(ActivitiOperateServiceImpl.class);

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.newer.integration.ActivitiOperateService#
	 * deployProcessByZipInputStream(java.util.zip.ZipInputStream)
	 */
	public BaseResult deployProcessByZipInputStream(ZipInputStream zipInputStrem) {
		// 初始化返回結果
		BaseResult baseResult = new BaseResult();
		try {
			// 根據zip包發佈流程信息
			Deployment deployment = BaseBizFactory.getInstance().getRepositoryService().createDeployment()
					.addZipInputStream(zipInputStrem).deploy();
			// 添加返回結果信息
			baseResult.data.put("result", deployment);
			baseResult.success = true;
		} catch (Exception e) {
			// TODO: handle exception
			logger.error("查询流程定义信息失败,失败原因:" + e.getMessage(), e);
			baseResult.success = false;
			baseResult.msg = "查询流程定义信息失败";
		}
		return baseResult;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.newer.integration.ActivitiOperateService#deployProcessByInputStream(
	 * java.io.InputStream)
	 */
	public BaseResult deployProcessByInputStream(String resourceName, InputStream inputStream) {
		// 初始化返回結果
		BaseResult baseResult = new BaseResult();
		try {
			Deployment deployment = BaseBizFactory.getInstance().getRepositoryService().createDeployment()
					.addInputStream(resourceName, inputStream).deploy();
			// 添加返回結果信息
			baseResult.data.put("result", deployment);
			baseResult.success = true;
		} catch (Exception e) {
			logger.error("根据bpmn流查询流程定义信息失败:" + e.getMessage(), e);
			baseResult.success = false;
			baseResult.msg = "根据bpmn流查询流程定义信息失败";
		}
		return baseResult;
	}

	public BaseResult deleteProcessByDeploymentId(String deploymentId) {
		// 初始化返回結果
		BaseResult baseResult = new BaseResult();
		if (StringUtils.isBlank(deploymentId)) {
			baseResult.success = false;
			baseResult.msg = "流程定义id的参数信息异常,请检查参数信息.";
			baseResult.code = "10001";
			logger.warn("刪除流程图失败,失败原因:流程定义id的参数信息异常,请检查参数信息.");
			return baseResult;
		}
		try {
			BaseBizFactory.getInstance().getRepositoryService().deleteDeployment(deploymentId);
			baseResult.success = true;
		} catch (Exception e) {
			logger.error("刪除流程定义信息失败:" + e.getMessage(), e);
			baseResult.success = false;
			baseResult.msg = "刪除流程定义信息失败";
		}
		return baseResult;
	}

}
