package com.newer.service.runtime;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.newer.BaseBizFactory;
import com.newer.integration.runtime.RuntimeOperateService;
import com.newer.result.BaseResult;

public class RuntimeOperateServiceImpl implements RuntimeOperateService {
	// 日志信息打印
	private static final Logger logger = LoggerFactory.getLogger(RuntimeOperateServiceImpl.class);

	/*
	 * (non-Javadoc)
	 * @see com.newer.integration.runtime.RuntimeOperateService#startProcess(java.lang.String)
	 */
	public BaseResult startProcess(String processDefinenationKey,String businessKey,Map<String,Object> variables) {
		BaseResult baseResult = new BaseResult();
		// 判断蚕食信息是否正确
		if (StringUtils.isBlank(processDefinenationKey)) {
			baseResult.success = false;
			baseResult.msg = "参数信息异常,请检查参数信息.";
			baseResult.code = "10001";
			logger.warn("流程启动时key信息失败,失败原因:参数信息异常,请检查参数信息.");
			return baseResult;
		}
		try {
			BaseBizFactory.getInstance().getRuntimeService().startProcessInstanceByKey(processDefinenationKey,variables);
		} catch (Exception e) {
			logger.error("流程启动失败,失败原因:" + e.getMessage(), e);
			baseResult.success = false;
			baseResult.msg = "流程启动失败";
		}
		return baseResult;
	}

	public BaseResult cancelProcess(String departmentId) {
		return null;
	}
	
}
