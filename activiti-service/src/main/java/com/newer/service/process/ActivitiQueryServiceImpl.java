/**
 * 
 */
package com.newer.service.process;

import java.io.InputStream;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.activiti.engine.repository.ProcessDefinition;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;

import com.newer.BaseBizFactory;
import com.newer.integration.process.ActivitiQueryService;
import com.newer.result.BaseResult;

/**
 * activit信息查询服务实现
 * 
 * @author Administrator
 */
public class ActivitiQueryServiceImpl implements ActivitiQueryService {

	// 日志信息打印
	private static final Logger logger = LoggerFactory.getLogger(ActivitiQueryServiceImpl.class);

	/**
	 * 根据流程定义名称模糊查询流程信息
	 * 
	 * @see com.newer.integration.process.ActivitiQueryService#queryAllProcessDefinition(java
	 *      .lang.String)
	 */
	public BaseResult queryAllProcessDefinition(String definitionName) {
		BaseResult baseResult = new BaseResult();
		// 判断蚕食信息是否正确
		if (StringUtils.isBlank(definitionName)) {
			baseResult.success = false;
			baseResult.msg = "参数信息异常,请检查参数信息.";
			baseResult.code = "10001";
			logger.warn("查询流程定义信息失败,失败原因:参数信息异常,请检查参数信息.");
			return baseResult;
		}
		try {
			// TODO Auto-generated method stub
			// 根据流程定义名称返回结果
			List<ProcessDefinition> list = BaseBizFactory.getInstance().getRepositoryService()
					.createProcessDefinitionQuery().processDefinitionNameLike(definitionName).list();
			// 判断查询结果是否为空
			if (!CollectionUtils.isEmpty(list)) {
				baseResult.data.put("result", list);
			}
			baseResult.success = true;
		} catch (Exception e) {
			// TODO: handle exception
			logger.error("查询流程定义信息失败,失败原因:" + e.getMessage(), e);
			baseResult.success = false;
			baseResult.msg = "查询流程定义信息失败";

		}
		// 返回查询结果
		return baseResult;
	}
	/*
	 * (non-Javadoc)
	 * @see com.newer.integration.ActivitiQueryService#queryProcessImageById(java.lang.String)
	 */
	public BaseResult queryProcessImageById(String deploymentId) {
		BaseResult baseResult = new BaseResult();
		String resourceName=StringUtils.EMPTY;
		if (StringUtils.isBlank(deploymentId)) {
			baseResult.success = false;
			baseResult.msg = "流程定义id的参数信息异常,请检查参数信息.";
			baseResult.code = "10001";
			logger.warn("查询流程图失败,失败原因:流程定义id的参数信息异常,请检查参数信息.");
			return baseResult;
		}
		try {
			List<String> list=BaseBizFactory.getInstance().getRepositoryService().
				getDeploymentResourceNames(deploymentId);
			if(CollectionUtils.isEmpty(list)){
				baseResult.success = false;
				baseResult.msg = "流程定义id的对应的流程图不存在,请检查参数信息.";
				return baseResult;
			}
			for (String name : list) {
				if(name.indexOf(".png") >= 0){
					resourceName = name;
				}
			}
			InputStream in = BaseBizFactory.getInstance().getRepositoryService().getResourceAsStream(deploymentId, resourceName);
			if(in!=null){
				baseResult.data.put("result",in);
			}
			baseResult.success = true;
		} catch (Exception e) {
			logger.error("查询流程图信息失败,失败原因:" + e.getMessage(), e);
			baseResult.success = false;
			baseResult.msg = "查询流程图信息失败";
		}
		return baseResult;
	}
	/*
	 * (non-Javadoc)
	 * @see com.newer.integration.ActivitiQueryService#queryLastVersionProcessDefinition()
	 */
	public BaseResult queryLastVersionProcessDefinition() {
		BaseResult baseResult = new BaseResult();
		try {
			List<ProcessDefinition> list = BaseBizFactory.getInstance().getRepositoryService().createProcessDefinitionQuery()
					.orderByProcessDefinitionVersion().asc().list();
			Map<String,ProcessDefinition> map = new HashMap<String, ProcessDefinition>();
			if(!CollectionUtils.isEmpty(list)){
				for (ProcessDefinition pd : list) {
					map.put(pd.getKey(), pd);
				}
			}
			baseResult.data.put("result", map);
			baseResult.success = true;
		} catch (Exception e) {
			logger.error("查询流程图信息查询失败,失败原因:" + e.getMessage(), e);
			baseResult.success = false;
			baseResult.msg = "查询流程图信息查询失败";
		}
		return null;
	}
	/*
	 * (non-Javadoc)
	 * @see com.newer.integration.ActivitiQueryService#queryProcessViewPic(java.lang.String)
	 */
	public BaseResult queryProcessViewPic(String deploymentId) {
		BaseResult baseResult = new BaseResult();
		if (StringUtils.isBlank(deploymentId)) {
			baseResult.success = false;
			baseResult.msg = "流程定义id的参数信息异常,请检查参数信息.";
			baseResult.code = "10001";
			logger.warn("查询流程图失败,失败原因:流程定义id的参数信息异常,请检查参数信息.");
			return baseResult;
		}
		try {
			List<String> list= BaseBizFactory.getInstance().getRepositoryService().
					getDeploymentResourceNames(deploymentId);
			String resourceName = StringUtils.EMPTY;
			if(CollectionUtils.isEmpty(list)){
				for (String name : list) {
					if(name.indexOf(".png")>=0){
						resourceName = name;
					}
				}
			}
			InputStream in = BaseBizFactory.getInstance().getRepositoryService().
					getResourceAsStream(deploymentId, resourceName);

			baseResult.success = true;
			baseResult.data.put("result", in);
		} catch (Exception e) {
			logger.error("查询流程图Pic失败,失败原因:" + e.getMessage(), e);
			baseResult.success = false;
			baseResult.msg = "查询流程图pic失败";
		}
		return baseResult;
	}
	
}
