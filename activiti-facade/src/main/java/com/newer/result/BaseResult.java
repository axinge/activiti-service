/**
 * 
 */
package com.newer.result;

import java.util.HashMap;
import java.util.Map;

/**
 * 服务信息统一返回结果
 * 
 * @author Administrator
 */
public class BaseResult {
	// 是否成功
	public boolean success;

	// 返回code
	public String code;

	// 返回失败信息
	public String msg;

	// 返回结果集合
	public Map<String, Object> data = new HashMap();

	public BaseResult() {
	}
}
