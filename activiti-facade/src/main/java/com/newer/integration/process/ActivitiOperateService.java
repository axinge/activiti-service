package com.newer.integration.process;

import java.io.InputStream;
import java.util.zip.ZipInputStream;

import com.newer.result.BaseResult;

/**
 * 流程操作相关服务
 * 
 * @author imac
 */
public interface ActivitiOperateService {

	/**
	 * 根据zip包发布流程
	 * 
	 * @param zipInputStrem
	 *            多个或者一个流程图通过压缩成为zip包,获取zip流文件发布流程信息
	 * @return
	 */
	public BaseResult deployProcessByZipInputStream(ZipInputStream zipInputStrem);

	/**
	 * 根据bpmn流，来发布流程
	 * @param inputStream  是一个bpmn流
	 * @return 
	 */
	public BaseResult deployProcessByInputStream(String resourceName,InputStream inputStream);
	
	/**
	 * 
	 * @param deploymentid 流程编号
	 * @return 
	 */
	public BaseResult deleteProcessByDeploymentId(String deploymentid);
}
