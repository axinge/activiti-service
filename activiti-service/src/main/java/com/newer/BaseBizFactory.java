/**
 * 
 */
package com.newer;

import org.activiti.engine.ProcessEngine;
import org.activiti.engine.ProcessEngines;

/**
 * @author Administrator
 * 
 */
public class BaseBizFactory {

	static ProcessEngine processEngine = null;
	static Class<BaseBizFactory> baseBizFactory = BaseBizFactory.class;
	/**
	 * 获取流程引擎对象
	 * 
	 * @return
	 */
	public static ProcessEngine getInstance() {
		synchronized (baseBizFactory) {
			if (processEngine == null) {
				// 创建核心引擎对象
				processEngine = ProcessEngines.getDefaultProcessEngine();
			}
			return processEngine;
		}
	}

}
