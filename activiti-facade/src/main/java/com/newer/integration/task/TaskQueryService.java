package com.newer.integration.task;

import com.newer.result.BaseResult;

public interface TaskQueryService {
	/**
	 * @param person 根据人来查询任务
	 * @return
	 */
	public BaseResult findTaskByPerson(String person);
}
