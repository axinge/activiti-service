package com.newer.integration.runtime;

import java.util.Map;

import com.newer.result.BaseResult;

public interface RuntimeOperateService {
	/**
	 * 根据流程定义的key启动对应流程
	 */
	public BaseResult startProcess(String processDefinenationKey,String businessKey,Map<String,Object> variables);
	/**
	 * 根据流程定义的id取消对应流程
	 */
	public BaseResult cancelProcess(String departmentId);
}
