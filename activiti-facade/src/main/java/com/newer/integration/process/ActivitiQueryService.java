/**
 * 
 */
package com.newer.integration.process;

import java.util.List;

import com.newer.result.BaseResult;

/**
 * @author Administrator
 *
 */
public interface ActivitiQueryService {

	/**
	 * 根据流程定义名称模糊查询流程信息
	 * 
	 * @param definitionName
	 *            流程定义名称
	 * @return
	 */
	public BaseResult queryAllProcessDefinition(String definitionName);
	/**
	 * 根据流程定义的id查询某一个对应的流程图
	 * @param deploymentId 流程定义的id
	 * @return
	 */
	public BaseResult queryProcessImageById(String deploymentId);
	
	/**
	 * 查询所有流程定义的最新版本的集合
	 * @return
	 */
	public BaseResult queryLastVersionProcessDefinition();
	/**
	 * 查询指定流程定义id对应的流程图
	 */
	public BaseResult queryProcessViewPic(String deploymentId);
	
	/**
	 * 
	 */
}
