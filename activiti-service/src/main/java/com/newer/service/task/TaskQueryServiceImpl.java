package com.newer.service.task;

import java.util.List;

import org.activiti.engine.task.Task;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.newer.BaseBizFactory;
import com.newer.integration.task.TaskQueryService;
import com.newer.result.BaseResult;

public class TaskQueryServiceImpl implements TaskQueryService {

	// 日志信息打印
	private static final Logger logger = LoggerFactory.getLogger(TaskQueryServiceImpl.class);

	/*
	 * (non-Javadoc)
	 * @see com.newer.integration.task.TaskQueryService#findTaskByPerson(java.lang.String)
	 */
	public BaseResult findTaskByPerson(String person) {
		BaseResult baseResult = new BaseResult();
		// 判断蚕食信息是否正确
		if (StringUtils.isBlank(person)) {
			baseResult.success = false;
			baseResult.msg = "参数信息异常,请检查参数信息.";
			baseResult.code = "10001";
			logger.warn("查询任务信息失败,失败原因:参数信息person异常,请检查参数信息.");
			return baseResult;
		}
		try {
			List<Task> list = BaseBizFactory.getInstance().getTaskService().createTaskQuery()
					.taskAssigneeLike(person).list();
			baseResult.data.put("result", list);
		} catch (Exception e) {
			logger.error("根据person查询任务失败,失败原因:" + e.getMessage(), e);
			baseResult.success = false;
			baseResult.msg = "查询任务失败";
		}
		return baseResult;
	}

}
